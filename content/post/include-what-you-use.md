---
title: "Include What You Use"
date: 2017-12-05T14:06:35-05:00
categories:
  - cpp
tags:
  - programming
  - macOS
  - cpp
---

Include-what-you-use is a promising little tool for cleaning up a codebase. It didn't end up working for the use I had for it, but it still could be useful. Here is a quick guideline on installing it on macOS.

<!--more-->

## Install to a brew llvm install

First, install the latest LLVM suite using brew (`brew install llvm`). You'll want to activate this install; I use Lmod ([the files here](https://github.com/CLIUtils/envmodule_setup/blob/master/macOS/Core/llvm)). Pay attention to what version you end up getting.

Next, you'll follow the guidelines on a out-of-source install. Basically, that amounts to:

```bash
mkdir IWYU
cd IWYU
git clone https://github.com/include-what-you-use/include-what-you-use.git
cd include-what-you-use
git checkout clang_5.0 # Or whatever you got with brew
cd ..
mkdir build
cd build
cmake ../include-what-you-use -DCMAKE_INSTALL_PREFIX=$(brew --prefix llvm) -DIWYU_LLVM_ROOT_PATH=$(brew --prefix llvm)
make
make install
```

(If you are using fish instead of bash, remove the `$`)

## Usage

Then, to use in another CMake project:

```bash
cmake .. -DCMAKE_CXX_INCLUDE_WHAT_YOU_USE=$(which include-what-you-use)
make 2> iwyu.out
fix_includes.py -n --nosafe_headers --comments < iwyu.ou
```

(remove the `-n` to actually make the changes)

That's all there is to it! It, unfortunately, broke the software I was trying to clean up, but the idea is great.
